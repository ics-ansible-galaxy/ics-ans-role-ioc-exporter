import pytest


def test_ioc_exporter_is_enabled_and_running(host):
    exporter = "ioc-exporter"
    service = host.service(exporter)
    assert service.is_enabled
    assert service.is_running

    assert host.file(f"/var/log/prometheus/{exporter}.log").exists


@pytest.mark.parametrize("tag_type", ["annotated", "lightweight"])
def test_ioc_exporter(host, tag_type):
    ioc_exporter_port = "12110"
    cmd = host.command(f"curl localhost:{ioc_exporter_port}")

    if "no-iocs" not in host.ansible.get_variables()["inventory_hostname"]:
        # Check that all metrics are there even though there were exceptions because of
        # fake IOCs
        assert 'ioc_cellmode_used{ioc="annotated-clean"} 0.0' in cmd.stdout

        # Clean
        assert f'ioc_repository_dirty{{ioc="{tag_type}-clean"}} 0.0' in cmd.stdout
        assert (
            f'ioc_repository_local_commits{{ioc="{tag_type}-clean"}} 0.0' in cmd.stdout
        )

        # Dirty
        assert f'ioc_repository_dirty{{ioc="{tag_type}-dirty"}} 1.0' in cmd.stdout
        assert (
            f'ioc_repository_local_commits{{ioc="{tag_type}-dirty"}} 0.0' in cmd.stdout
        )

        # Local commit
        assert (
            f'ioc_repository_dirty{{ioc="{tag_type}-local-commit"}} 0.0' in cmd.stdout
        )
        assert (
            f'ioc_repository_local_commits{{ioc="{tag_type}-local-commit"}} 1.0'
            in cmd.stdout
        )


def test_logs(host):
    if "no-iocs" not in host.ansible.get_variables()["inventory_hostname"]:
        assert (
            "Failure trying to identify git repository: /opt/iocs/incorrect"
            in host.file("/var/log/prometheus/ioc-exporter.log").content_string
        )

def test_ioc_exporter_was_shutdown_by_watchdog(host):
    cmd = host.run("journalctl -u ioc-exporter")
    assert cmd.rc == 0
    assert "ioc-exporter.service watchdog timeout" in cmd.stdout
